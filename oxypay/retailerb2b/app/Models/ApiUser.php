<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiUser extends BaseModel {


    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $hidden = ['password', 'activation_link'];
    protected $guarded = ['created_at', 'updated_at', 'id'];
    

}
