<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use JWTAuth;
use Hash;
use Auth;
use Validator;
use Helper;

class UserController extends ApiBaseController {
    /*
      |--------------------------------------------------------------------------
      | User Controller
      |--------------------------------------------------------------------------
      | Author : Naru Lal keer | kundanroy
      | This controller is responsible for handling user login, register, token related api response.
      |
     */

    public function index() {
        return parent::output('success', 'User: API NOT FOUND.');
    }


    public function validateUser(Request $request, User $user) {

        $input['first_name'] = $request->input('first_name');
        $input['last_name'] = $request->input('last_name');
        $input['email'] = $request->input('email');
        $input['password'] = Hash::make($request->input('password'));
        //Server side valiation
        if ($request->input('user_id')) {
            $validator = Validator::make($request->all(), [
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                        'email' => 'required|email|unique:users'
            ]);
        }

        // Return Error Message
        if ($validator->fails()) {
            $error_msg = [];
            foreach ($validator->messages()->all() as $key => $value) {
                array_push($error_msg, $value);
            }

            return parent::output('failed', $error_msg[0], $input, 500);
        }

        $helper = new Helper;
        $group_name = $helper->getCorporateGroupName($input['email']);
        $email_allow = array('gmail', 'yahoo', 'ymail', 'aol', 'hotmail');

        if (in_array($group_name, $email_allow)) {
            return parent::output('failed', 'Only corporate email is allowed!', $input, 500);
        }
        return parent::output('success', "User validated successfully.", $request->all(), 200);
    }

    public function register(Request $request) {
        $input['first_name'] = $request->input('first_name');
        $input['last_name'] = $request->input('last_name');
        $input['email'] = $request->input('email');
        $input['mobile'] = $request->input('mobile');
        $input['username'] = $request->input('username');
        $input['role_type'] = ($request->input('role_type')) ? $request->input('role_type') : '';

        if ($request->input('user_id')) {
            $u = $this->updateProfile($request, $user);
            return $u;
        }

        //Server side valiation
        $validator = Validator::make($request->all(), [
                    'username' => 'required|unique:users',
                    'email' => 'required|email|unique:users',
                    'mobile' => 'required|unique:users',
                    'first_name' => 'required',
                    'password' => 'required'
        ]);
        /** Return Error Message * */
        if ($validator->fails()) {
            $error_msg = [];
            foreach ($validator->messages()->all() as $key => $value) {
                array_push($error_msg, $value);
            }

            //return parent::output('failed', $error_msg[0], $input, 403);
        }
        $data = $input;
        /** --Create USER-- * */
        $user = new \App\Models\ApiUser;
        parent::prepare($user, $request, 'users');
        $user->user_type = self::$default_user_type;
        $user->password = bcrypt($request->input('password'));
        $user->status =1;
        try {
            \DB::statement('SET FOREIGN_KEY_CHECKS=0;');           
            $user->save();
            \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
            $user = \App\Models\ApiUser::find($user->id);
            if ($token = JWTAuth::fromUser($user)) { //token done
                //Lets SaveProfile
                 $userProfile = \App\Models\UserProfile::where('user_id',$user->id)->first();
                 $profile = [
                      'first_name'=>$input['first_name'],
                      'last_name'=>$input['last_name'],
                      'user_id'=>$user->id,
                      'status'=>1,
                          ];
                 if(!$userProfile){
                $userProfile = \App\Models\UserProfile::create($profile);
                 }else{
                    $userProfile->update($profile);   
                 }
            }
            $data['token'] = $token;
            $data['user_id'] = $user->id;
            return parent::output('success', "Thank you for registration. Please verify your email.", $data, 200);
        } catch (\Exception $e) {

            return parent::output('failed', $e->getMessage().'Something worg with data. Please check data and try again.', $data, 500);
        }
    }

    public function login(Request $request) {
        $username = $request->get('username');
        $token ='';
        if(filter_var($username,FILTER_VALIDATE_EMAIL)){
        $token = JWTAuth::attempt(['email' => $username, 'password' => $request->get('password')]);
        }
        
        if(!$token){
         $token = JWTAuth::attempt(['username' => $username, 'password' => $request->get('password')]);           
        }
        if (!$token) {
            return response()->json(["status" => "failed", "code" => 403, "message" => "Invalid Username or password. Try again!", 'data' => '']);
        }

        $user = JWTAuth::toUser($token);

        $data['user_id'] = $user->id;
        $data['token'] = $token;

        return response()->json(["status" => "success", "code" => 200, "message" => "Successfully logged in.", 'data' => $data]);
    }

 

    public function logout(Request $request) {
        $token = $request->input('token');

        JWTAuth::invalidate($request->input('token'));
        return parent::output('success', "You've successfully signed out.");
    }
}
