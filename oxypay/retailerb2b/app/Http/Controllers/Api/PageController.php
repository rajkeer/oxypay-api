<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use Illuminate\Http\Request;

class PageController extends ApiBaseController {
 
    public function index($slug = null) {
        return parent::output('success', $slug . ' page found.');
    }

    public function welcome($slug = null) {
        return parent::output('success', 'Welcome to LogMyDay API.',[]);
    }

    public function page404($slug = null) {
        return parent::output('failed', $slug . ' API NOT FOUND.', [], 404);
    }

    public function page403($slug = null) {
        return parent::output('failed', $slug . ' Access Denied.', [], 403);
    }

}
