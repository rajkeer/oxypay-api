<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;

class ApiBaseController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | API BASE Controller
      |--------------------------------------------------------------------------
      | Author : Naru Lal keer | kundanroy
      | This controller is responsible for handling all api response, emails, helper methods.
      |
     */

    protected $errors = array();
    protected $currentUserId = 0;
	static public $default_user_type = 3;//DEVELOPER
    protected $accessToken = '';
    protected $API_KEY = '';
    protected $apiHelper;
    static public $limit = 20;
    static public $default_tasklog_status = 1;

    public function __construct(Request $request) {
        if ($request->header('Content-Type') != "application/json") {
            $request->headers->set('Content-Type', 'application/json');
        }
        $this->currentUserId = $request->input('userID');
    }

    public function getCurrentUserID(Request $request){
        try{
        $user = \JWTAuth::toUser($request->input('token'))->toArray();
        $this->currentUserId = $user['id'];
        return $user['id'];
        } catch (\Exception $e){
          $this->currentUserId =0;  
        }
        return 0;
    }

    static public function prepare(&$baseModel, Request $request, $table, $except = ['id', 'created_at', 'updated_at', 'status']) {

        $table_cname = \Schema::getColumnListing($table);
        foreach ($table_cname as $key => $value) {

            if (in_array($value, $except)) {
                continue;
            }
            $baseModel->$value = $request->get($value);
        }
    }

    static public function output($status = 1, $message = null, $data = [], $code = 200) {

        $response = [];
        $response['status'] = $status;
        $response['code'] = $code;
        $response['message'] = $message;
        $response['data'] = $data;
        return Response::json($response,$code);
    }

}
