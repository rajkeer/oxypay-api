<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Validator;

class PaymentController extends ApiBaseController {

    public function index(Request $request) {
        $user_id = $request->get('user_id');
        $page_number = $request->get('page_number');
        $page_size = ($request->get('page_limit')) ? $request->get('page_limit') : parent::$limit;
        if ($page_number > 1) {
            $offset = $page_size * ($page_number - 1);
        } else {
            $offset = 0;
        }
        $payment = \App\Models\Payment::where(function($q) use($user_id) {
                    if ($user_id) {
                        $q->where('user_id', $user_id);
                    }
                });
        $total_count = $payment->count();
        if ($page_number > 1) {
            $payments = $payment->skip($offset)->take($page_size)->get()->toArray();
        } else {
            $payments = $payment->get()->toArray();
        }

        $data = array('count' => $total_count, 'payments' => $payments);
        return parent::output('success', 'Payments List found', $data);
    }

    public function saveUPIPayment (Request $request) {
        $post_request = $request->all();
        //Server side valiation
        $validator = Validator::make($request->all(), [
                    'name' => 'required',
					'mobile' => 'required',
					'otxn_amount' => 'required',
					'otxn_id' => 'required',
					
        ]);
		
		/*$post_data =array(
		'name'=>'',	
		'ouser_id'=>'',
		'mobile'=>'', 
		'otxn_id'=>'',
		'otxn_note'=>'',
		'otxn_amount'=>'',
		'otxn_currency'=>'',
		'rupi_id'=>'',
		'rupi_name'=>'',
		'upi_txnRef'=>'',
		'upi_txnId'=>'',
		'upi_txnStatus'=>'',
		'upi_responseCode'=>'',
		'upi_approvalRefNo'=>'',
		'upi_txnDate');*/
		
		
        /** Return Error Message * */
        if ($validator->fails()) {
            $error_msg = [];
            foreach ($validator->messages()->all() as $key => $value) {
                array_push($error_msg, $value);
            }
            return parent::output('failed', $error_msg[0], $post_request, 500);
        }
		
        $payment = new \App\Models\Payment;
		parent::prepare($payment, $request, 'oxypay_upi_payments');
		

        $data = [];
        try {
            \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            $payment->save();
            \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

            $status = 'success';
            $code = 200;
            $message = 'Payment save successfully.';
            $data = $payment;
        } catch (\Exception $e) {
           dd($e);
            $status = 'failed';
            $code = 500;
            $message = 'Payment save failed.';
        }

        return parent::output($status, $message, $data,$code);
    }


    public function getPayments(Request $request, $user_id = 0) {

        $page_number = $request->get('page_number');
        $page_size = ($request->get('page_limit')) ? $request->get('page_limit') : parent::$limit;
        if ($page_number > 1) {
            $offset = $page_size * ($page_number - 1);
        } else {
            $offset = 0;
        }
        $payment = \App\Models\Payment::where(function($q) use($user_id) {
                    if ($user_id) {
                        $q->where('user_id', $user_id);
                    }
                });
        $total_count = $payment->count();
        if ($page_number > 1) {
            $payments = $payment->skip($offset)->take($page_size)->get()->toArray();
        } else {
            $payments = $payment->get()->toArray();
        }

        $data = array('count' => $total_count, 'payments' => $payments);
        return parent::output('success', 'Payments List found', $data);
    }

    public function getMyPayment(Request $request) {
        self::getCurrentUserID($request);
        $user_id = $this->currentUserId;
        return self::getPayments($request, $user_id);
    }
}
