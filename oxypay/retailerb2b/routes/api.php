<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With, auth-token');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
// Non Version Routes
Route::get('/', 'Api\PageController@welcome');
Route::get('/v1', 'Api\PageController@welcome');

//Routes for version 1
Route::prefix('v1')->namespace('Api')->group(function () {
    Route::get('/', function () {
        return array('success' => true, 'message' => 'Welcome to api page -version 1.0.', 'code' => 200);
    });

    Route::post('user/login', 'UserController@login');
    Route::get('user/logout', 'UserController@logout');
    Route::post('user/register', 'UserController@register');

    $api_middleware =[];// ['jwt-auth'];
    Route::middleware($api_middleware)->group(function () {
        Route::post('payments/upi/save', 'PaymentController@saveUPIPayment');
        Route::post('payments/update', 'PaymentController@updatePayment');
        Route::get('payments', 'PaymentController@index');

       
    });

    Route::get('{any}', function ($any) {
        return App::make('\App\Http\Controllers\Api\PageController')->page404($any);
    })->where('any', '.*');
});