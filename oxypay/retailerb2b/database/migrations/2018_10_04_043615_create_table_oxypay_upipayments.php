<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOxypayUpipayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oxypay_upi_payments', function (Blueprint $table) {
            $table->increments('id');
			
			$table->string('name',32)->nullable();
			$table->integer('ouser_id')->nullable();
			$table->string('mobile',16);
			
			$table->string('otxn_id',64);
			$table->string('otxn_note',256)->nullable();
			$table->decimal('otxn_amount',10,2)->nullable()->default('0.00');
			$table->string('otxn_currency',4)->nullable()->default('INR');

			
			$table->string('rupi_id',64)->nullable();
			$table->string('rupi_name',64)->nullable()->default('OXYPAY');

			
			$table->string('upi_txnRef',64)->nullable();
			$table->string('upi_txnId',64)->nullable();
			$table->string('upi_txnStatus',16)->nullable();
			$table->string('upi_responseCode',16)->nullable();
			$table->string('upi_approvalRefNo',64)->nullable();
            $table->timestamp('upi_txnDate')->nullable();
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oxypay_upi_payments');
    }
}
