<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOxypayAuthkeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oxypay_authkeys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->default(0)->nullable();
            $table->string('key',64)->unique();
			$table->tinyInteger('api_version')->default(1);
			$table->integer('total_access')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('oxypay_authkeys');
    }
}
